FROM node:12.22-alpine
WORKDIR /var/www
RUN  npm install bootstrap
# RUN npm install -g @vue/cli
COPY package.json ./
RUN  npm install
EXPOSE 8080
CMD ["npm", "run", "serve"]



# docker-composer up # to start the compilation process
# to install npm package run
# docker-compose exec vue-prod npm i bootstrap
#sudo docker exec  docker-vuejs_web_1 sh -c "npm install vue bootstrap bootstrap-vue"
# docker exec  docker-vuejs_web_1 sh -c "npm install vue-router@next"
