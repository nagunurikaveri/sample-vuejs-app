import HomePage from './components/HomePage.vue';
import SignUp from './components/SignUp.vue';
import LogIn from './components/Login.vue';
import Posts from './components/Posts.vue';
import AddPost from './components/AddPost.vue';
import EditPost from './components/EditPost.vue';

import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        name: 'HomePage',
        component: HomePage,
        path: '/'
    },
    {
        name: 'SignUp',
        component: SignUp,
        path: '/sign-up'
    },
    {
        name: 'LogIn',
        component: LogIn,
        path: '/login'
    },
    {
        name: 'Posts',
        component: Posts,
        path: '/posts'
    },
    {
        name: 'AddPost',
        component: AddPost,
        path: '/add-post'
    },
    {
        name: 'EditPost',
        component: EditPost,
        path: '/edit-post/:id?'
    }
];
const router = createRouter({
    history: createWebHistory(),
    routes

})
export default router;