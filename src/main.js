import { createApp } from 'vue'
import App from './App.vue'
import router from './routers'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
window.$ = window.jQuery = require('jquery');

createApp(App).use(router).mount('#app')
