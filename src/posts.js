import axios from "axios";
const UserInfo = JSON.parse(document.cookie);


export default axios.create({
    baseURL: "http://127.0.0.1:82/api/posts",
    headers: {
        Authorization: `Bearer ${UserInfo.token}`,
    }
});